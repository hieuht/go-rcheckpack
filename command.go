package main

import (
	"strings"

	"golang.org/x/crypto/ssh"
)

func getUptime(client *ssh.Client) (string, error) {
	uptime, err := runCommand(client, "/bin/cat /proc/uptime")
	showError(err)
	return uptime, err
}

func getDistro(client *ssh.Client) (string, error) {
	result, err := runCommand(client, "/bin/cat /etc/issue")
	showError(err)
	lines := strings.Split(result, "\n")
	distro := strings.TrimSpace(lines[0])
	return distro, err
}
