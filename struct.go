package main

//Server strucuture
type Server struct {
	ip       string `max:"15"`
	port     int64
	username string
	key      string
	os       string
}
