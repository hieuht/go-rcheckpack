package main

import (
	"bufio"
	"os"
	"strconv"
	"strings"
)

func showError(err error) {
	if err != nil {
		panic(err)
	}
}

func parseServerFromFile() []Server {
	file, err := os.Open(serverConfig)
	defer file.Close()
	showError(err)
	var servers []Server
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		// Read line by line and split it into parameters
		line := scanner.Text()
		paras := strings.Split(line, "|")
		// Convert the parameters to Server strucuture
		var server Server
		server.ip = paras[0]
		server.port, err = strconv.ParseInt(paras[1], 10, 32)
		showError(err)
		server.username = paras[2]
		server.key = paras[3]
		// Add each server to server list
		servers = append(servers, server)
	}
	return servers
}
