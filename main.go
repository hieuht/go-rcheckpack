package main

import "fmt"

func main() {
	// Init Database
	db := InitDB(dbPath)
	if db != nil {
		createTableServer(db)
		createTablePackage(db)
	}

	// Test commandsß
	servers := parseServerFromFile()
	var updateServers []Server
	for _, server := range servers {
		addr := fmt.Sprintf("%s:%d", server.ip, server.port)
		fmt.Printf("Processing Server: %s\n", addr)
		username := server.username
		key := server.key
		client := sshConnect(username, addr, key)
		distro, _ := getDistro(client)
		server.os = distro
		updateServers = append(updateServers, server)
	}
	addServer(db, updateServers)
	defer db.Close()

}
