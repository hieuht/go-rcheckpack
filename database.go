package main

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

// init database
func InitDB(filepath string) *sql.DB {
	db, err := sql.Open("sqlite3", filepath)
	showError(err)
	if db == nil {
		panic("db nil")
	}
	return db
}

// create table server
func createTableServer(db *sql.DB) {
	sql := `
    CREATE TABLE IF NOT EXISTS server(
      Id INTEGER PRIMARY KEY AUTOINCREMENT,
  		Ip TEXT NOT NULL,
  		Port TEXT NOT NULL,
      Username TEXT NOT NULL,
      PathKey TEXT NOT NULL,
      OS TEXT NOT NULL,
  		InsertedDatetime DATETIME NOT NULL
    );
  `
	_, err := db.Exec(sql)
	showError(err)
}

// create table package and service
func createTablePackage(db *sql.DB) {
	sql := `
    CREATE TABLE IF NOT EXISTS package(
      Id INTEGER PRIMARY KEY AUTOINCREMENT,
      ServerId TEXT NOT NULL,
      Name TEXT NOT NULL,
    　Version TEXT NOT NULL,
      Status TEXT NOT NULL
    );
  `
	_, err := db.Exec(sql)
	showError(err)
}

// add server
func addServer(db *sql.DB, servers []Server) {
	sql := `
  INSERT OR REPLACE INTO server(
		Ip,
		Port,
    Username,
    PathKey,
    OS,
		InsertedDatetime
	) values(?, ?, ?, ?, ?, CURRENT_TIMESTAMP);
  `
	stmt, err := db.Prepare(sql)
	showError(err)
	defer stmt.Close()
	for _, server := range servers {
		_, err2 := stmt.Exec(server.ip, server.port, server.username, server.key, server.os)
		showError(err2)
	}
}
